# IRIS_SYSTEM_TASKS



## Tasks 
| Task | Branch | Description |   
| ------------- | ------------- | ------------ |
| 1 | [Task 1](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-1)  |  Pack the rails application in a docker container image.  | 
| 2 | [Task 2](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-2) | Launch a separate container for the database and ensure that the two containers are able to connect. |
| 3 | [Task 3](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-3) | Launch an Nginx container, and configure it as a reverse proxy to the rails application | 
| 4 | [Task 4](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-4) | Launching two more application containers and configuring nginx container to load balance incoming requests |
| 5 | [Task 6](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-6) | Using docker compose to run the containers together using a single command |
| 6 | [Task 7](https://github.com/likithraj4444/IRIS_SYSTEM_RECRUITMENT_221ME328/tree/TASK-7) |  Adding requests rate limit |   
| 7 |
